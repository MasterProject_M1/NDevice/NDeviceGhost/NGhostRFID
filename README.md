Ghost RFID
==========

Introduction
------------

This project aims to read a RFID reader, and send read data to
event listener.

RFID data model
---------------

9600 bauds

|STX|XXXXXXXXXXXX|CR OU LF|LF|ETX|

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostRFID.git

